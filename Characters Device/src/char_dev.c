#include <linux/fs.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/uaccess.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Fernando Franca");
MODULE_DESCRIPTION("Simple characters device");
MODULE_VERSION("1.0.0");

static char kern_buf[100]; // Bytes
static int dev_open(struct inode* inode, struct file* file);
static ssize_t dev_read(
    struct file* file, char* user_buf, size_t len, loff_t* off
);
static ssize_t dev_write(
    struct file* file, const char* user_buf, size_t len, loff_t* off
);
static int dev_release(struct inode* inode, struct file* file);

static struct file_operations fops = {
    .read=dev_read,
    .write=dev_write,
    .open=dev_open,
    .release=dev_release
};

static int __init chrdev_init(void) {
    // 90 is the major ID used to create symlinks
    int t = register_chrdev(90, "my_char_dev", &fops);

    if (t < 0) {
        printk(KERN_ERR "Error registering the character device\n");

        return -EIO;
    }

    printk(KERN_INFO "Character device registered\n");

    return 0;
}

static void __exit chrdev_exit(void) {
    unregister_chrdev(90, "my_char_dev");
    printk(KERN_INFO "Character device unregistered\n");
}

static int dev_open(struct inode* inode, struct file* file) {
    printk(KERN_INFO "Device opened\n");

    return 0;
}

static ssize_t dev_read(
    struct file* file, char* user_buf, size_t len, loff_t* off
) {
    if (*off >= sizeof(kern_buf)) return 0;

    // Prevent buffer overflow
    len = len > sizeof(kern_buf) ? sizeof(kern_buf) : len;

    // Copy from Kernel space to User space
    copy_to_user(user_buf, kern_buf, len);

    return len;
}

static ssize_t dev_write(
    struct file* file, const char* user_buf, size_t len, loff_t* off
) {
    // Prevent buffer overflow
    if (len >= sizeof(kern_buf)) return -EIO;

    // Clear the buffer
    memset(kern_buf, 0, sizeof(kern_buf));

    // Copy from User space to Kernel space
    copy_from_user(kern_buf, user_buf, len);

    kern_buf[len] = 0;

    return len;
}

static int dev_release(struct inode* inode, struct file* file) {
    printk(KERN_INFO "Device closed\n");

    return 0;
}

module_init(chrdev_init);
module_exit(chrdev_exit);

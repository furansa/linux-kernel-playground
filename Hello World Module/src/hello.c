#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Fernando Franca");
MODULE_DESCRIPTION("Hello world Kernel module");
MODULE_VERSION("1.0.0");

static int __init hello_init(void) {
    printk(KERN_INFO "Hello Linux\n");

    return 0;
}

static void __exit hello_exit(void) {
    printk(KERN_INFO "Goodbye Linux\n");
}

module_init(hello_init);
module_exit(hello_exit);
